#pragma once
#include "Vector.h"
#include <time.h>
#include<fstream>
#include <string>
#include<iostream>
#include <iomanip>
#include <cmath>
using namespace std;

class Matrix
{
private:
	int M; 
	int N;
	Vector *matrix;
public:
	Matrix( int m,int n);
	Matrix();
	Matrix(const Matrix& m );

	friend ostream& operator<<(ostream& stream, const Matrix& m);
	friend istream& operator>>(istream& stream, Matrix& m);

	void DowlandFromFile();
	void Generator();
	Vector& operator[](int index);
	
	Matrix operator+(double num);
	Matrix operator-(double num);
	Matrix operator*(double num);
	Matrix operator/(double num);

	Matrix& operator+=(double num);
	Matrix& operator-=(double num);
	Matrix& operator*=(double num);
	Matrix& operator/=(double num);

	Matrix operator+(const Matrix& m2);
	Matrix operator-(const Matrix& m2);
	Matrix operator*(const Matrix& m2);

	Matrix& operator+=(const Matrix& m2);
	Matrix& operator-=(const Matrix& m2);

	Matrix& operator=(const Matrix& m2);
	bool operator==(const Matrix& m2) const;
	bool operator!=(const Matrix& m2) const;

	Matrix Transpose();
	double Determinant();
	Matrix Minor(int m, int n);
	Matrix Inverse();
	~Matrix();
};


