#include "Vector.h"

Vector::Vector()
{
	N = 0;
	vector = nullptr;
}

Vector::Vector(int N)
{
	this->N = N;
	vector = new double[N];
	for(int i=0;i<N;i++)
	{
		vector[i] = 0;
	}
}

Vector::Vector(const Vector & v)
{
	this->N = v.N;
	this->vector = new double[N];
	for (int i = 0; i < N; i++)
		this->vector[i] = v.vector[i];
}


double & Vector::operator[](int index)
{
	if (index < 0 || index >= N)
	{
		cout << "�������� ������";
		exit(EXIT_FAILURE);
	}
	return vector[index];
}

const double & Vector::operator[](int index) const
{
	return vector[index];
}

Vector Vector::operator+(const Vector & v)
{
		Vector tmp(N);
		for (int i = 0; i < N; i++)
			tmp.vector[i] = vector[i] + v.vector[i];
		return tmp;
}

Vector Vector::operator-(const Vector & v)
{
	Vector tmp(N);
	for (int i = 0; i < N; i++)
		tmp.vector[i] = vector[i] - v.vector[i];
	return tmp;
}

Vector Vector::operator+(double num)
{
	Vector tmp(N);
	for (int i = 0; i < N; i++)
		tmp.vector[i] = vector[i] + num;
	return tmp;
}

Vector Vector::operator-(double num)
{
	Vector tmp(N);
	for (int i = 0; i < N; i++)
		tmp.vector[i] = vector[i] - num;
	return tmp;
}

Vector Vector::operator*(double num)
{
	Vector tmp(N);
	for (int i = 0; i < N; i++)
		tmp.vector[i] = vector[i] * num;
	return tmp;
}

Vector Vector::operator/(double num)
{
	Vector tmp(N);
	for (int i = 0; i < N; i++)
		tmp.vector[i] = vector[i] / num;
	return tmp;
}

Vector & Vector::operator=(const Vector & v)
{
	if (this != &v)
	{
		if (N != v.N)
		{
			delete[] vector;
			N = v.N;
			vector = new double[v.N];
			for (int i = 0; i < N; i++)
				vector[i] = v.vector[i];
		}
		if (N == 0)
		{
			vector = new double[v.N];
			for (int i = 0; i < N; i++)
				vector[i] = v.vector[i];
		}
		for (int i = 0; i < N; i++)
			vector[i] = v.vector[i];
	}
	return *this;
}

Vector & Vector::operator+=(const Vector & v)
{
	if (this != &v)
		for(int i=0;i<N;i++)
			vector[i] += v.vector[i];
	return *this;
}

Vector & Vector::operator-=(const Vector & v)
{
	if (this != &v)
		for (int i = 0; i<N; i++)
			vector[i] -=  v.vector[i];
	return *this;
}

Vector & Vector::operator+=(double num)
{
	for (int i = 0; i<N; i++)
		vector[i] +=num;
	return *this;
}

Vector & Vector::operator-=(double num)
{
	for (int i = 0; i<N; i++)
		vector[i] -= num;
	return *this;
}

Vector & Vector::operator*=(double num)
{
	for (int i = 0; i<N; i++)
		vector[i] *= num;
	return *this;
}

Vector & Vector::operator/=(double num)
{
	for (int i = 0; i<N; i++)
		vector[i] /= num;
	return *this;
}

bool Vector::operator==(const Vector & v) const
{
	for(int i=0;i<N;i++)
		if(vector[i]!=v.vector[i])
	return false;
		else return true;
}

bool Vector::operator!=(const Vector & v) const
{
	for (int i = 0; i<N; i++)
		if (vector[i] == v.vector[i])
			return false;
		else return true;
}

Vector::~Vector()
{}

ostream & operator<<(ostream & stream, const Vector & v)
{
	for (int i = 0; i < v.N; i++)
	{
		stream << setfill(' ') << setw(10) << left;
		stream << v.vector[i]<<" ";
	}
	stream << endl<<endl;
	return stream;
}

istream & operator>>(istream & stream, Vector & v)
{
	for (int i = 0; i < v.N; i++)
		stream >> v.vector[i];
	return stream;
}
