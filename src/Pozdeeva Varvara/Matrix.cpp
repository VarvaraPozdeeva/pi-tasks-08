#include "Matrix.h"

Matrix::Matrix(int m, int n)
{
	this->M = m;
	this->N = n;
	matrix = new Vector[M];
	for (int i = 0; i < M; i++)
		matrix[i] = Vector(N);
}

Matrix::Matrix()
{
	this->M = 0;
	this->N = 0;
	matrix = nullptr;
}

Matrix::Matrix(const Matrix & m)
{
	this->M = m.M;
	this->N = m.N;
	this->matrix = new Vector[M];
	for (int i = 0; i < M; i++)
		matrix[i] = Vector(m.matrix[i]);
}

ostream & operator<<(ostream & stream, const Matrix & m)
{
	for (int i = 0; i < m.M; i++)
	{
		stream << m.matrix[i];
	}
	return	stream;
}

istream & operator>>(istream & stream, Matrix & m)
{
	for (int i = 0; i < m.M; i++)
	{
		stream >> m.matrix[i];
	}
	return	stream;
}

void Matrix::DowlandFromFile()
{
	ifstream file("Matrix.txt");
	if (!file.is_open())
	{
		cout << "File don't open!" << endl;
	}
	while (!file.eof())
	{
		int n, m;
		file >> m >> n;
		this->N = n;
		this->M = m;
		this->matrix = new Vector[M];
		for (int i = 0; i < M; i++)
		{
			matrix[i] = Vector(N);
			for (int j = 0; j < N; j++)
			{
				file >> this->matrix[i][j];
			}
		}
	}
	file.close();
}

void Matrix::Generator()
{
	for (int i = 0; i<M; i++)
		for (int j = 0; j < N; j++)
		{
			matrix[i][j] = rand() % 10;
		}
}

Vector & Matrix::operator[](int index)
{
	if (index < 0 || index >= M)
	{
		cout << "�������� ������";
		exit(EXIT_FAILURE);
	}
	return matrix[index];
}

Matrix Matrix::operator+(double num)
{
	Matrix	tmp(M, N);
	for (int i = 0; i<M; i++)
		tmp.matrix[i] = this->matrix[i] + num;
	return tmp;
}

Matrix Matrix::operator-(double num)
{
	Matrix	tmp(M, N);
	for (int i = 0; i<M; i++)
		tmp.matrix[i] = this->matrix[i] - num;
	return tmp;
}

Matrix Matrix::operator*(double num)
{
	Matrix	tmp(M, N);
	for (int i = 0; i<M; i++)
		tmp.matrix[i] = this->matrix[i] * num;
	return tmp;
}

Matrix Matrix::operator/(double num)
{
	if (num == 0)
	{
		cout << "�� 0 ������ ������!" << endl;
		exit(EXIT_FAILURE);
	}
	Matrix	tmp(M, N);
	for (int i = 0; i<M; i++)
		tmp.matrix[i] = this->matrix[i] / num;
	return tmp;
}

Matrix & Matrix::operator+=(double num)
{
	for (int i = 0; i < M; i++)
		matrix[i] += num;
	return *this;
}

Matrix & Matrix::operator-=(double num)
{
	for (int i = 0; i < M; i++)
		matrix[i] -= num;
	return *this;
}

Matrix & Matrix::operator*=(double num)
{
	for (int i = 0; i < M; i++)
		matrix[i] *= num;
	return *this;
}

Matrix & Matrix::operator/=(double num)
{
	if (num == 0)
	{
		cout << "�� 0 ������ ������!" << endl;
		exit(EXIT_FAILURE);
	}
	for (int i = 0; i < M; i++)
		matrix[i] /= num;
	return *this;
}

Matrix Matrix::operator+(const Matrix & m2)
{
	if (this->M != m2.M || this->N != m2.N)
	{
		cout << "������! ������ ������� ������." << endl;
		exit(EXIT_FAILURE);
	}
	Matrix	tmp(M, N);
	for (int i = 0; i < M; i++)
			tmp.matrix[i] = this->matrix[i]+ m2.matrix[i];
	return tmp;
}

Matrix Matrix::operator-(const Matrix & m2)
{
	if (this->M != m2.M || this->N != m2.N)
	{
		cout << "������! ������ ������� ������." << endl;
		exit(EXIT_FAILURE);
	}
	Matrix	tmp(M, N);
	for (int i = 0; i < M; i++)
		tmp.matrix[i] = this->matrix[i] - m2.matrix[i];
	return tmp;
}

Matrix Matrix::operator*(const Matrix & m2)
{
	if (N!= m2.M )
	{
		cout << "������! �������� ������� ������." << endl;
		exit(EXIT_FAILURE);
	}
	Matrix	tmp(M, m2.N);
	for (int i = 0; i < M; i++)
		for(int j=0;j<m2.N;j++)
			for(int k=0; k<N; k++)
				tmp.matrix[i][j] += matrix[i][k] * m2.matrix[k][j];
	return tmp;
}

Matrix & Matrix::operator+=(const Matrix & m2)
{
	if (this->M != m2.M || this->N != m2.N)
	{
		cout << "������! ������ ������� ������." << endl;
		exit(EXIT_FAILURE);
	}
	for (int i = 0; i < M; i++)
		this->matrix[i]+= m2.matrix[i];
	return *this;
}

Matrix & Matrix::operator-=(const Matrix & m2)
{
	if (this->M != m2.M || this->N != m2.N)
	{
		cout << "������! ������ ������� ������." << endl;
		exit(EXIT_FAILURE);
	}
	for (int i = 0; i < M; i++)
		this->matrix[i] -= m2.matrix[i];
	return *this;
}

Matrix & Matrix::operator=(const Matrix & m2)
{
	if (this != &m2)
	{
		for (int i = 0; i < M; i++)
			matrix[i] = m2.matrix[i];
	}
	return *this;
}

bool Matrix::operator==(const Matrix & m2) const
{
	if (this->M != m2.M || this->N != m2.N)
	{
		cout << "������! ������ ������� ������." << endl;
		exit(EXIT_FAILURE);
	}
	for (int i = 0; i < M; i++)
		if (this->matrix[i] != m2.matrix[i])
			return false;
	return true;
}

bool Matrix::operator!=(const Matrix & m2) const
{
	if (this->M != m2.M || this->N != m2.N)
	{
		cout << "������! ������ ������� ������." << endl;
		exit(EXIT_FAILURE);
	}
	for (int i = 0; i < M; i++)
		if (this->matrix[i] == m2.matrix[i])
			return false;
	return true;
}

Matrix Matrix::Transpose()
{
	Matrix tmp(N, M);
	for (int i = 0; i < tmp.M; i++)
		for (int j = 0; j < tmp.N; j++)
			tmp.matrix[i][j] = matrix[j][i];
		return tmp;
}

double Matrix::Determinant()
{
	if (M != N)
	{
		cout << "������! ������� �� ����������." << endl;
		exit(EXIT_FAILURE);
	}
	if (M == 1)
		return matrix[0][0];
	if (M == 2)
		return matrix[0][0] * matrix[1][1] - matrix[1][0] * matrix[0][1];
	double d=0;
	for (int i = 0, j = 0; i < M; i++)
		d += matrix[i][j] * pow(-1.0,2.0+i) *(this->Minor(i, j).Determinant());
	return d;
}

Matrix Matrix::Minor(int m, int n)
{
	Matrix tmp(M - 1, N - 1);
	int itmp=0, jtmp=0;
	for (int i = 0; i < tmp.M; i++)
	{
		if (i == m)
			itmp = 1;
		jtmp = 0;
		for (int j = 0; j < tmp.N; j++)
		{
			if (j == n)
				jtmp = 1;
			tmp.matrix[i][j] = matrix[i + itmp][j + jtmp];
		}
	}
	return tmp;
}

Matrix Matrix::Inverse()
{
	if (M == N)
	{	
		Matrix tmp(M, N);
		double d = this->Determinant();
		if (d != 0)
		{
			Matrix alad(M, N);//������� �� �������������� ����������
			for (int i = 0; i < M; i++)
				for (int j = 0; j < N; j++)
				{
					alad.matrix[i][j] = (this->Minor(i, j).Determinant())*pow(-1.0, i + j);
				}
			d = 1 / d;
			return tmp = alad.Transpose()*d;
		}
		else
		{
			cout << "������! ������� �� ��������, ������������ ����� 0.";
			exit(EXIT_FAILURE);
		}
	}
	else
	{
		cout << "������! ������� �� ��������, ��� �� �������� ����������.";
		exit(EXIT_FAILURE);
	}
}

Matrix::~Matrix()
{}
