#pragma once
#include <fstream>
#include <iostream>
#include <iomanip>
using namespace std;
class Vector
{
private:
	double *vector;
	int N;//���������� ��������� 
public:
	Vector();
	Vector(int N);
	Vector(const Vector& v);

	friend ostream& operator<<(ostream& stream, const Vector& v);
	friend istream& operator>>(istream& stream, Vector& v);
	double& operator[](int index);
	const double& operator[](int index) const;

	Vector operator+(const Vector& v);
	Vector operator-(const Vector& v);
	Vector operator+(double num);
	Vector operator-(double num);
	Vector operator*(double num);
	Vector operator/(double num);

	Vector& operator=(const Vector& v);
	Vector& operator+=(const Vector& v);
	Vector& operator-=(const Vector& v);
	
	Vector& operator+=(double num);
	Vector& operator-=(double num);
	Vector& operator*=(double num);
	Vector& operator/=(double num);

	bool operator==(const Vector& v) const;
	bool operator!=(const Vector& v) const;
		
	~Vector();
};

